from UI import UI
import sys
import urllib.request


def load_page(url):
    http_response = None
    try:
        http_response = urllib.request.urlopen(url)
    except Exception as e:
        return str(e)
    finally:
        if http_response:
            return http_response.read()
        else:
            return None


args = sys.argv
ui = UI(args, load_page)
